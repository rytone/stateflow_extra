use std::fmt;
use time::PreciseTime;
use stateflow::with_init::Pipeline;

pub trait Profile: Pipeline {
	fn profiled_and<S>(self, next: S) -> ProfiledAnd<Self, S>
	where
		S: Pipeline<Input = Self::Output>,
		Self: Sized,
	{
		ProfiledAnd {
			this: self,
			next: next,
		}
	}
}

pub struct ProfiledAnd<S, N>
where
	S: Pipeline,
	N: Pipeline<Input = S::Output>,
{
	this: S,
	next: N,
}

impl<S, N> fmt::Debug for ProfiledAnd<S, N>
where
	S: Pipeline,
	N: Pipeline<Input = S::Output>,
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		self.next.fmt(f)
	}
}

impl<S, N> Pipeline for ProfiledAnd<S, N>
where
	S: Pipeline,
	N: Pipeline<Input = S::Output>,
{
	type Input = S::Input;
	type Output = N::Output;

	fn init(&mut self, input: Self::Input) -> Self::Output {
		self.next.init(self.this.init(input))
	}


	fn exec(&mut self, input: Self::Input) -> Self::Output {
		let this_res = self.this.exec(input);

		let next_name = format!("{:?}", self.next);
		let st = PreciseTime::now();
		let next_res = self.next.exec(this_res);
		let et = PreciseTime::now();

		println!("{} - {}", next_name, st.to(et));

		next_res
	}
}

impl<S, N> Profile for ProfiledAnd<S, N>
where
	S: Pipeline,
	N: Pipeline<Input = S::Output>,
{
}
